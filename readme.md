# django三方包


## 安装

```bash
pip install git+https://gitlab.com/sazima/django_common.git@master
```

## 配置settings.py


- 缓存(可选)

```python
CACHE_SETTINGS = {
    'HOST': os.environ.get('REDIS_HOST', '127.0.0.1'),
    'PORT': os.environ.get('REDIS_PORT', '6379'),
    'DB': os.environ.get('REDIS_DB', 1),
    'TIME_OUT': os.environ.get('REDIS_TIME_OUT', -1),
    'PREFIX': os.environ.get('REDIS_PREFIX', '3ddemo::')
}
```
- mongodb(可选)
```python
# mongodb连接参数
MONGODB_CONN = {
    'host': os.environ.get('MONGO_HOST', '127.0.0.1'),
    'port': 27017,
    'db': '3ddemo'
}
from mongoengine import connect
# mongo数据库连接
connect(
    MONGODB_CONN.get('db', '3ddemo'),
    host=MONGODB_CONN.get('host'),
    port=MONGODB_CONN.get('port')
)
```

- 日志(可选)
```python
# logging日志配置
logger_path = '/tmp/3ddemo'
logger_file = 'logs.log'
os.makedirs(logger_path, exist_ok=True)
full_logger_path = os.path.join(logger_path, logger_file)
if not os.path.isfile(full_logger_path):
    with open(full_logger_path, 'w') as f:
        f.write('')
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'verbose': {
            'format': '[%(asctime)s] [%(levelname)s] %(message)s'
        },
    },
    'handlers': {
        # 输出日志的控制台
        'console': {
            'level': 'INFO',
            'class': 'logging.StreamHandler',
            'formatter': 'verbose'
        },
        # 输出日志到文件，按日期滚动
        'file': {
            'level': 'INFO',
            'class': 'logging.handlers.TimedRotatingFileHandler',
            # TimedRotatingFileHandler的参数
            # 参照https://docs.python.org/3/library/logging.handlers.html#timedrotatingfilehandler
            # 目前设定每天一个日志文件
            'filename': full_logger_path,
            'when': 'midnight',
            'interval': 1,
            'backupCount': 10,
            'formatter': 'verbose'
        },

        'email': {
            'level': 'ERROR',
            'class': 'django.utils.log.AdminEmailHandler',
            'include_html': True,
        }
    },
    'loggers': {
        # 不同的logger
        'django': {
            'handlers': ['console', 'file'],
            'level': 'DEBUG',
            'propagate': True,
        },
    },
}

```
