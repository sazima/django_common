from setuptools import setup

setup(
    name='shendi-django-common',
    version='0.0.3',
    author='xx',
    author_email='xx@gmail.com',
    url='http://shendi3d.com',
    description='xx ',
    packages=['shendi_django_common'],
    # long_description=readme,
    long_description_content_type="text/markdown",
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ], install_requires=['pymongo', 'mongoengine', 'django<=3', 'djangorestframework', 'redis']
)
