from typing import Optional, List, Dict, Any

from mongoengine import get_db, DEFAULT_CONNECTION_NAME
from pymongo.cursor import Cursor
from pymongo.database import Database


class PageResult:
    DEFAULT_PAGE = 1
    DEFAULT_PAGE_SIZE = 15

    def __init__(self):
        self.results: list = list()
        self.count = 0

    @classmethod
    def page_queryset(cls, queryset, serializer_class=None, parameters: dict = None) -> 'PageResult':
        page, page_size = cls.get_page_args(parameters)
        count = queryset.count()
        start = (page - 1) * page_size
        results = queryset.all()[start:start + page_size]
        page_result = cls()
        page_result.count = count
        if serializer_class:
            serializer = serializer_class(results, many=True)
            page_result.results = serializer.data
        else:
            page_result.results = results
        return page_result

    @classmethod
    def page_mongo_query(cls, collection: str, query: dict, order_by: tuple = None,
                         parameters: dict = None, alias=DEFAULT_CONNECTION_NAME) -> 'PageResult':
        page, page_size = cls.get_page_args(parameters)
        alias = alias
        db_collection = getattr(cls._get_mongodb(alias), collection)
        cursor: Cursor = db_collection.find(query)
        if order_by is not None:
            cursor: Cursor = cursor.sort(order_by)
        cursor = cursor.skip((page - 1) * page_size).limit(page_size)
        count: int = db_collection.count(query)
        results: List[Dict[str, Any]] = list()
        for item in cursor:
            pk = item.pop('_id')
            item['id'] = str(pk)
            results.append(item)
        page_result = cls()
        page_result.count = count
        page_result.results = results
        return page_result

    @classmethod
    def _get_mongodb(cls, alias) -> Database:
        return get_db(alias)

    @classmethod
    def get_page_args(cls, data: Optional[dict]) -> (int, int):
        data = data or dict()
        page = data.get('page')
        page_size = data.get('page_size')
        try:
            page = int(page)
        except (ValueError, TypeError):
            page = cls.DEFAULT_PAGE
        try:
            page_size = int(page_size)
        except (ValueError, TypeError):
            page_size = cls.DEFAULT_PAGE_SIZE
        return page, page_size

    @property
    def data(self) -> dict:
        return {
            'count': self.count,
            'results': self.results
        }
