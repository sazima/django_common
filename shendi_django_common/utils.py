import datetime
import decimal
import logging
import uuid
from threading import current_thread

from django.core.serializers.json import DjangoJSONEncoder
from django.db import models
from django.db.models import QuerySet
from django.utils.duration import duration_iso_string
from django.utils.functional import Promise
from django.utils.timezone import is_aware

logger = logging.getLogger('django')
logger.info('start success ...')

all_request = dict()


def get_current_request():
    return all_request.get(current_thread().ident)


class JSONEncoder(DjangoJSONEncoder):
    def default(self, o):
        if isinstance(o, models.Model):
            o_fields = getattr(o, 'fields', None)
            if not o_fields:
                o_fields = [k.name for k in o._meta.fields]
            return {k: getattr(o, k) for k in o_fields}
        elif isinstance(o, datetime.datetime):
            return o.strftime('%Y-%m-%d %H:%M:%S')
        elif isinstance(o, datetime.date):
            return o.strftime('%Y-%m-%d')
        elif isinstance(o, datetime.time):
            if is_aware(o):
                raise ValueError("JSON can't represent timezone-aware times.")
            r = o.isoformat()
            if o.microsecond:
                r = r[:12]
            return r
        elif isinstance(o, datetime.timedelta):
            return duration_iso_string(o)
        elif isinstance(o, (decimal.Decimal, uuid.UUID, Promise)):
            return str(o)
        elif isinstance(o, set):
            return list(o)
        elif isinstance(o, QuerySet):
            return [x for x in o]
        else:
            return super().default(o)
