import pickle
from typing import Type, TypeVar, Any

from django.conf import settings
from django.db import models
from redis import ConnectionPool, Redis

# model类class
M = TypeVar('M', bound=models.Model)


class BaseCache:
    cache_settings = settings.CACHE_SETTINGS
    key_prefix = cache_settings.get('PREFIX', '')
    default_time_out = cache_settings.get('TIME_OUT')

    @classmethod
    def get_redis_pool(cls) -> ConnectionPool:
        if not hasattr(cls, '_redis_pool'):
            cls._redis_pool = ConnectionPool(host=settings.CACHE_SETTINGS.get('HOST', ),
                                             port=settings.CACHE_SETTINGS.get('PORT'),
                                             db=settings.CACHE_SETTINGS.get('DB'))
        return cls._redis_pool

    @classmethod
    def client(cls) -> Redis:
        if not hasattr(cls, '_client'):
            cls._client = Redis(connection_pool=cls.get_redis_pool())
        return cls._client

    @classmethod
    def dump_object(cls, value) -> bytes:
        """Dumps an object into a string for redis.  By default it serializes
        integers as regular string and pickle dumps everything else.
        """
        t = type(value)
        if t == int:
            return str(value).encode("ascii")
        return b"!" + pickle.dumps(value)

    @classmethod
    def load_object(cls, value: bytes) -> Any:
        """The reversal of :meth:`dump_object`.  This might be called with
        None.
        """
        if value is None:
            return None
        if value.startswith(b"!"):
            try:
                return pickle.loads(value[1:])
            except pickle.PickleError:
                return None
        try:
            return int(value)
        except ValueError:
            # before 0.8 we did not have serialization.  Still support that.
            return value

    @classmethod
    def get_by_pk(cls, pk, model: Type[M]) -> M:
        # 从缓存中获取, 如果没有就从数据库种读取, 并放到缓存
        key = '{}::pk::{}'.format(model.__name__, pk)
        data = cls.get(key)
        if not data:
            try:
                data = model.objects.get(pk=pk)
                cls.set(key, data)
            except model.DoesNotExist:
                data = None
        return data

    @classmethod
    def set_by_instance(cls, instance: M, model: Type[M]):
        """添加缓存"""
        key = '{}::pk::{}'.format(model.__name__, str(instance.id))
        cls.set(key, instance)

    @classmethod
    def delete_by_pk(cls, pk, model: Type[M]):
        """删除"""
        key = '{}::pk::{}'.format(model.__name__, pk)
        return cls.delete(key)

    @classmethod
    def delete(cls, key):
        """根据键删除"""
        return cls.strict_delete(cls.get_key_with_prefix(key))

    @classmethod
    def get(cls, key):
        """获取并解码"""
        return cls.load_object(cls.strict_get(cls.get_key_with_prefix(key)))

    @classmethod
    def set(cls, key, value, timeout=None):
        if timeout is None:
            timeout = cls.default_time_out
        dump = cls.dump_object(value)
        key_with_prefix = cls.get_key_with_prefix(key)
        if timeout == -1:
            result = cls.strict_set(key_with_prefix, value=dump)
        else:
            result = cls.strict_setex(key_with_prefix, value=dump, timeout=timeout)
        return result

    # 严格模式
    @classmethod
    def strict_get(cls, key):
        return cls.client().get(key)

    @classmethod
    def strict_set(cls, key, value):
        return cls.client().set(key, value=value)

    @classmethod
    def strict_setex(cls, key, value, timeout):
        return cls.client().setex(key, value=value, time=timeout)

    @classmethod
    def strict_delete(cls, key):
        return cls.client().delete(key)

    @classmethod
    def get_key_with_prefix(cls, key):
        return '{}{}'.format(cls.key_prefix, key)
