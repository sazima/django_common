import traceback
from threading import current_thread

from django.http import JsonResponse
from django.utils.deprecation import MiddlewareMixin
from rest_framework.response import Response

# from utils.auth_util import token_to_user
from .response import ApiException
from .utils import logger, JSONEncoder, all_request


class ResponseMiddleWare(MiddlewareMixin):
    api_status_map = {
        201: '添加成功',
        204: '删除成功',
        200: '成功',
    }

    def process_response(self, request, response):
        all_request.pop(current_thread().ident, None)
        if isinstance(response, Response):
            code = response.status_code  # http状态码
            if code >= 400:
                # rest framework的异常
                response_data = {
                    'code': ApiException.status_code,
                    'msg': response.data.get('detail') or response.data
                }
                logger.info({
                    'response': response_data
                })
                return JsonResponse(response_data)
            # rest framework的成功
            return JsonResponse({
                'code': 200,  # 统一使用200
                'msg': self.api_status_map.get(response.status_code, '成功'),
                'info': response.data
            }, encoder=JSONEncoder)
        # django自带的响应
        return response

    def process_exception(self, request, exception):
        """统一格式"""
        all_request.pop(current_thread().ident, None)
        if isinstance(exception, ApiException):
            result = {
                'code': exception.status_code,
                'msg': exception.msg,
            }
            if exception.info:
                result.setdefault('info', exception.info)
            return JsonResponse(result)
        elif isinstance(exception, Exception):
            logger.error(traceback.format_exc())

    def process_request(self, request):
        all_request.setdefault(current_thread().ident, request)
