import json

from django.http import JsonResponse

from .utils import JSONEncoder
from django.conf import settings

if 'shendi_django_common.middleware.ResponseMiddleWare' not in settings.MIDDLEWARE:
    settings.MIDDLEWARE.append('shendi_django_common.middleware.ResponseMiddleWare')
print(settings.MIDDLEWARE)


class ApiException(Exception):
    status_code = 405000
    msg = '系统错误'
    info = None

    def __init__(self, msg=None, status_code=None, info=None):
        if status_code:
            self.status_code = status_code
        if msg:
            self.msg = msg
        if info:
            self.info = info

    def to_dict(self):
        return {
            'code': self.status_code,
            'msg': self.msg,
            'info': self.info
        }

    def dumps(self):
        return json.dumps(self.to_dict())


class ParamsError(ApiException):
    status_code = 405001
    msg = '参数错误'

    def __init__(self, msg=None, status_code=None, info=None, field=None):
        if info and field:
            info = {field: info}

        super(ParamsError, self).__init__(status_code=status_code, msg=msg, info=info)


class TokenError(ApiException):
    status_code = 405002
    msg = "未登录"


class AuthorityError(ApiException):
    status_code = 405003
    msg = "无权限"


class NotFoundError(ApiException):
    status_code = 405004
    msg = '无此项目'


class TimeError(ApiException):
    status_code = 405009
    msg = "敬请期待"


class StatusError(ApiException):
    status_code = 405011
    message = '状态有误'


class Success(JsonResponse):
    def __init__(self, info=None, encoder=JSONEncoder, safe=True,
                 json_dumps_params=None, msg='成功', **kwargs):
        data = {
            'code': 200,
            'msg': msg,
            'info': info
        }
        super(Success, self).__init__(data, encoder, safe, json_dumps_params, **kwargs)

    def dumps(self):
        return self.content.decode()
