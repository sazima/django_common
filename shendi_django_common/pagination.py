from collections import OrderedDict
from typing import List, Sequence

from rest_framework.pagination import PageNumberPagination
from rest_framework.response import Response
from rest_framework.settings import api_settings


class PageNumberSizePagination(PageNumberPagination):
    """
    分页:
        http://api.example.org/accounts/?page=4
        http://api.example.org/accounts/?page=4&page_size=100
    """
    page_size = api_settings.PAGE_SIZE
    page_size_query_param = 'page_size'
    page_query_param = 'page'
    max_page_size = None

    def get_paginated_response(self, data):
        return Response(OrderedDict([
            ('count', self.page.paginator.count),
            ('results', data)
        ]))

