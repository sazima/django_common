import datetime

from django.db import models


class BaseModel(models.Model):
    create_time = models.DateTimeField(default=datetime.datetime.now, verbose_name='创建时间')
    update_time = models.DateTimeField(auto_now=True, verbose_name='更新时间')

    class Meta:
        abstract = True

    def update(self, **kwargs):
        for k, v in kwargs.items():
            setattr(self, k, v)
        return self

    def copy_properties(self, source):
        for field in self._meta.get_fields():
            field_name = getattr(field, 'name')
            if field_name in ['update_time', 'create_time', 'id']:
                continue
            try:
                setattr(self, field_name, getattr(source, field_name))
            except AttributeError:
                pass
        return self
